#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2022
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.4.y-cip"

SRC_URI[sha256sum] = "186949d20d7ca0856d666665d66782805c39d6811a6baab1b88af1690c6c73e1"
