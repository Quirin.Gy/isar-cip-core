#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.19.y-cip"

SRC_URI[sha256sum] = "770db530cb68f0eb022757fde73774b7312d420fcfc3d86d906f37208bdbce2d"
